#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <X11/Xft/Xft.h>
#include <X11/Xlib.h>

#include "modules.h"
#include "i3module.h"
#define USE_I3 1

//#ifdef USE_I3

//TODO nyancat module
/* Get the width and height of the window `w' */
void
get_wh(Display *d, Window *w, int *width, int *height)
{
  XWindowAttributes win_attr;
  
  XGetWindowAttributes(d, *w, &win_attr);
  if (height != NULL)
    *height = win_attr.height;

  if (width != NULL)
    *width = win_attr.width;
}

int
text_extents(unsigned char *str, int len, Display *d, XftFont *font, int *ret_width, int *ret_height)
{
	int	height;
	int	width;
	XGlyphInfo	gi;
	XftTextExtentsUtf8(d, font, str, len, &gi);
	height = gi.y - gi.height ;//- gi.y;
	width = gi.width - gi.x;
	if (ret_width != NULL)  *ret_width = width;
	if (ret_height != NULL) *ret_height = height;
	return width;
}

void
draw (Display *d, Window w, GC bg, XftDraw *xd, XftFont *font, int width, int height, struct state *s)
{
  char batteria [15];
  int percentuale = s->bat;
  snprintf (batteria, 15, "BAT %d%% %s", percentuale,
	    s->in_carica ? "+" : "-");

  XFillRectangle(d, w, bg, 0, 0, width, height);
  int a, l;
  text_extents((unsigned char*)s->data, strlen(s->data), d, font, &l, &a);

  int ww = 0;
  int ws = 10;
  for (int i = 0; i < 10; ++i)
    if (s->w[i].name != NULL)
      ww += ws + text_extents((unsigned char*)s->w[i].name, strlen(s->w[i].name), d, font, NULL, NULL);

  int wo =(width-ww)/2;
  for(int i=0; i<10; ++i)
    if(s->w[i].name != NULL){
      if (s->w[i].focused){
	if(s->w[i].urgent) {
	  XftDrawStringUtf8(xd, s->urgent, font, wo, 15,(unsigned char*)s->w[i].name, strlen(s->w[i].name));
	} else
	  XftDrawStringUtf8(xd, s->focused, font, wo, 15,(unsigned char*)s->w[i].name, strlen(s->w[i].name));
      } else
	XftDrawStringUtf8(xd, s->fg, font, wo, 15,(unsigned char*)s->w[i].name, strlen(s->w[i].name));
      wo += ws + text_extents((unsigned char*)s->w[i].name, strlen(s->w[i].name), d, font, NULL, NULL);
    }

  XftDrawStringUtf8(xd, s->fg, font, 10, 15, (unsigned char*)batteria,strlen((char*)batteria));
  XftDrawStringUtf8(xd, s->fg, font, width-l-10, 15, (unsigned char*)s->data, strlen(s->data));
  XFlush(d);
}

void
xorg_handle_events (Display *d, Window w, GC bg, XftDraw *xd, XftFont *font, int width, int height, struct state *s)
{
  //controllla tutti gli eventi già arrivati su XORG senza blocccare 
  while(XPending (d)){
    XEvent e;
    XNextEvent(d, &e);

    switch (e.type){
    case MapNotify:
    case ConfigureNotify:
    case Expose:
    case ClientMessage:
      get_wh(d, &w, &width, &height);
      draw(d, w, bg, xd, font, width, height, s);
      break;

    default:
      printf("Evento sconosciuto: %d\n", e.type);
    }
  }
  
}

/* Set some WM stuff */
void
set_win_atoms(Display *d, Window w, int width, int height){

  Atom atom;
  atom = XInternAtom(d, "_NET_WM_WINDOW_TYPE_DOCK", 0);

  XChangeProperty(d,
		  w,
		  XInternAtom(d, "_NET_WM_WINDOW_TYPE", 0),
		  XInternAtom(d, "ATOM", 0),
		  32,
		  PropModeReplace,
		  (unsigned char*)&atom, 1);

  atom = XInternAtom(d, "_NET_WM_STATE", 0);
  XChangeProperty(d,
		  w,
		  XInternAtom(d, "_NET_WM_STATE_STICKY", 0),
		  XInternAtom(d, "ATOM", 0),
		  32,
		  PropModeReplace,
		  (unsigned char*)&atom,
		  1);
}

int
main (void)
{
  Display              *d;
  Window               root, bar;
  XSetWindowAttributes attr;
  int                  width, height;
  XVisualInfo          vinfo;
  Colormap             cmap;
  XColor               f, v;
  GC                   bg;
  XftDraw              *xd;
  XftColor             fg, focused, urgent;
  XftFont              *font;

  /* Ignore the child status */
  signal(SIGCHLD, SIG_IGN);

  struct state s;
  s.w = calloc(10, sizeof(struct workspace));
  for (int i = 0; i < 10; ++i)
    s.w[i].name = NULL;
  if (!i3_setup(&s))
    return 1;

  if ((d = XOpenDisplay(NULL)) == NULL)
    return 1;

  root = DefaultRootWindow(d);
  get_wh(d, &root, &width, NULL);
  height = 20;

  XMatchVisualInfo(d, DefaultScreen(d), 32, TrueColor, &vinfo);

  cmap = XCreateColormap(d, root, vinfo.visual, AllocNone);

  attr.border_pixel = 0;
  attr.background_pixel = 0x90909090;
  attr.colormap = cmap;
  attr.event_mask = StructureNotifyMask | ExposureMask | VisibilityChangeMask;
  bar = XCreateWindow(d, root, 0, 0, width, height, 0, vinfo.depth, InputOutput, vinfo.visual, CWBorderPixel | CWBackPixel | CWColormap | CWEventMask, &attr);

  set_win_atoms(d, bar, width, height);

  XMapWindow (d, bar);

  bg = XCreateGC (d, bar, 0, NULL);
  XAllocNamedColor(d, cmap, "#ffffff", &f, &v);
  XSetForeground(d, bg, f.pixel);
  XSetBackground(d, bg, v.pixel);

  xd = XftDrawCreate(d, bar, vinfo.visual, cmap);

  {
    XRenderColor xr;
    xr.alpha = 0xFFFF;
    xr.red = 0x0000;
    xr.green = 0x0000;
    xr.blue = 0x0000;

    XftColorAllocValue(d, vinfo.visual, cmap, &xr, &fg);
    s.fg = &fg;

    //foregruond color

    xr.alpha = 0xffff;
    xr.red = 0xffff;
    xr.green = 0x0000;
    xr.blue = 0xffff;

    XftColorAllocValue(d, vinfo.visual, cmap, &xr, &focused);
    s.focused = &focused;

    // focused color

    xr.alpha = 0xffff;
    xr.red = 0xffff;
    xr.green = 0x0000;
    xr.blue = 0x0000;

    XftColorAllocValue(d, vinfo.visual, cmap, &xr, &urgent);
    s.urgent = &urgent;
    
  }

  font = XftFontOpenName(d, DefaultScreen(d), "DejaVu Mono:style=Book:size=10");

  batterymodule(&s);
  datemodule(&s);

  struct pollfd fds[2];
  fds[0].fd = ConnectionNumber(d);
  fds[0].events = POLLIN;
 //TODO: mettere un timeout sulla read
  fds[1].fd = s.i3fd_sub; 
  fds[1].events = POLLIN;

  printf("connection number = %d\n", ConnectionNumber(d));

  xorg_handle_events(d, bar, bg, xd, font, width, height, &s);
  while(1){
    int n = poll(fds, 2, 30000);
    printf("poll ke fa? %d\n", n);

    if(n == -1){
      perror("poll");
      break;
    }
    else if(n == 0){
      batterymodule(&s);
      datemodule(&s);
      printf("poll ke fa? %d\n", n);
    }

    if (fds[0].revents == POLLIN)
      xorg_handle_events(d, bar, bg, xd, font, width, height, &s);
    if (fds[1].revents == POLLIN) {
      i3_get_workspaces(&s);
    }

    draw(d, bar, bg, xd, font, width, height, &s);
  }

  /* TODO: deallocate xft stuff */

  i3_close(&s);
  free(s.w);
  XDestroyWindow(d, bar);
  XCloseDisplay(d);

  return 0;
}

//#endif
